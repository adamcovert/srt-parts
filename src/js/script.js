$(document).ready(function(){

  // Scroll to sections
  $(function(){
    $("a[href^='#']").click(function(){
      var _href = $(this).attr("href");
      $("html, body").animate({scrollTop: $(_href).offset().top+"px"});
      return false;
    });
  });

  // Header address modal
  // Address
  var modal = document.getElementById('address-modal');
  var btn = document.getElementById("address-button");
  var span = document.getElementsByClassName("address__modal-close")[0];
  btn.onclick = function() {
    modal.style.display = "block";
  }
  span.onclick = function() {
    modal.style.display = "none";
  }
  window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
  };

  // Woking time
  var modalTime = document.getElementById('address-modal--time');
  var btnTime = document.getElementById("address-button--time");
  var span = document.getElementsByClassName("address__modal-close--time")[0];
  btnTime.onclick = function() {
    modalTime.style.display = "block";
  }
  span.onclick = function() {
    modalTime.style.display = "none";
  }
  window.onclick = function(event) {
    if (event.target == modalTime) {
      modalTime.style.display = "none";
    }
  };

  var modalPhone = document.getElementById('address-modal--phone');
  var btnPhone = document.getElementById("address-button--phone");
  var span = document.getElementsByClassName("address__modal-close--phone")[0];
  btnPhone.onclick = function() {
    modalPhone.style.display = "block";
  }
  span.onclick = function() {
    modalPhone.style.display = "none";
  }
  window.onclick = function(event) {
    if (event.target == modalPhone) {
      modalPhone.style.display = "none";
    }
  };

  // Promo slider
  $('#promo-slider').owlCarousel({
    items: 1,
    loop: true,
    margin: 5,
    nav: false,
  });

  // Maintenance slider
  $('#maintenance-slider').owlCarousel({
    items: 4,
    loop: true,
    dots: false,
    nav: true,
    navText: ['<svg class="product-card-arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><g data-name="arrow" fill="#ffffff" stroke="#0b2076" stroke-miterlimit="10" stroke-width="2"><circle id="product-card-arrow__circle" cx="16" cy="16" r="15"/><path id="product-card-arrow__shape" stroke-linecap="round" d="M14.03 10.22L19.8 16M19.8 16l-5.77 5.78"/></g></svg>',
      '<svg class="product-card-arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><g data-name="arrow" fill="#ffffff" stroke="#0b2076" stroke-miterlimit="10" stroke-width="2"><circle id="product-card-arrow__circle" cx="16" cy="16" r="15"/><path id="product-card-arrow__shape" stroke-linecap="round" d="M14.03 10.22L19.8 16M19.8 16l-5.77 5.78"/></g></svg>'],
    margin: 20,
    stagePadding: 5,
    responsive: {
      0: {
        items: 1,
        center: true,
      },
      768: {
        items: 2,
      },
      1200: {
        items: 4,
      }
    }
  });

  // Elements slider
  $('#elements-slider').owlCarousel({
    items: 4,
    loop: true,
    dots: false,
    nav: true,
    navText: ['<svg class="product-card-arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><g data-name="arrow" fill="#ffffff" stroke="#0b2076" stroke-miterlimit="10" stroke-width="2"><circle id="product-card-arrow__circle" cx="16" cy="16" r="15"/><path id="product-card-arrow__shape" stroke-linecap="round" d="M14.03 10.22L19.8 16M19.8 16l-5.77 5.78"/></g></svg>',
      '<svg class="product-card-arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><g data-name="arrow" fill="#ffffff" stroke="#0b2076" stroke-miterlimit="10" stroke-width="2"><circle id="product-card-arrow__circle" cx="16" cy="16" r="15"/><path id="product-card-arrow__shape" stroke-linecap="round" d="M14.03 10.22L19.8 16M19.8 16l-5.77 5.78"/></g></svg>'],
    margin: 20,
    stagePadding: 5,
    responsive: {
      0: {
        items: 1,
        center: true,
      },
      768: {
        items: 2,
      },
      1200: {
        items: 4,
      }
    }
  });

  // Brake System slider
  $('#brake-system-slider').owlCarousel({
    items: 4,
    loop: true,
    dots: false,
    nav: true,
    navText: ['<svg class="product-card-arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><g data-name="arrow" fill="#ffffff" stroke="#0b2076" stroke-miterlimit="10" stroke-width="2"><circle id="product-card-arrow__circle" cx="16" cy="16" r="15"/><path id="product-card-arrow__shape" stroke-linecap="round" d="M14.03 10.22L19.8 16M19.8 16l-5.77 5.78"/></g></svg>',
      '<svg class="product-card-arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><g data-name="arrow" fill="#ffffff" stroke="#0b2076" stroke-miterlimit="10" stroke-width="2"><circle id="product-card-arrow__circle" cx="16" cy="16" r="15"/><path id="product-card-arrow__shape" stroke-linecap="round" d="M14.03 10.22L19.8 16M19.8 16l-5.77 5.78"/></g></svg>'],
    margin: 20,
    stagePadding: 5,
    responsive: {
      0: {
        items: 1,
        center: true,
      },
      768: {
        items: 2,
      },
      1200: {
        items: 4,
      }
    }
  });

  // Testimonials slider
  $('#testimonials-slider').owlCarousel({
    loop: true,
    dots: false,
    center: true,
    nav: true,
    navText: ['<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><g data-name="testimonials--arrow"><circle id="testimonials-arrow__circle" cx="25" cy="25" r="25" fill="#fff"/><path id="testimonials-arrow__shape" fill="none" stroke="#0b2076" stroke-width="4" stroke-miterlimit="10" d="M28.89 15.72l-9.9 9.9 9.9 9.9" stroke-linecap="square"/></g></svg>',
      '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><g data-name="testimonials--arrow"><circle id="testimonials-arrow__circle" cx="25" cy="25" r="25" fill="#fff"/><path id="testimonials-arrow__shape" fill="none" stroke="#0b2076" stroke-width="4" stroke-miterlimit="10" d="M28.89 15.72l-9.9 9.9 9.9 9.9" stroke-linecap="square"/></g></svg>'],
    responsive: {
      0: {
        items: 1,
      },
      768: {
        items: 3,
      },
      1200: {
        items: 5,
      }
    }
  });

});

// Google map
var mapPoints = [
  [
    'SRT-Parts',
    60.1425077,
    30.2110953
  ],
];

function initMap() {
  var mapDiv = document.getElementById('map');
  var center = {lat: 60.1425077, lng: 30.2110953};
  var map = new google.maps.Map(mapDiv, {
    zoom: 15,
    center: center,
    scrollwheel: false
  });

  setMapMarkers(map);

  map.addListener('resize', function() {
    map.panTo(center);
  });
  google.maps.event.addDomListener(window, 'resize', function(){
    google.maps.event.trigger(map, 'resize');
  });
}

function setMapMarkers(map) {
  var icon = {
    path: 'M40,0C26.191,0,15,11.194,15,25c0,23.87,25,55,25,55s25-31.13,25-55C65,11.194,53.807,0,40,0zM40,38.8c-7.457,0-13.5-6.044-13.5-13.5S32.543,11.8,40,11.8c7.455,0,13.5,6.044,13.5,13.5S47.455,38.8,40,38.8z',
    fillColor: '#1c0579',
    fillOpacity: 0.9,
    scale: 0.6,
    strokeColor: '#1c0579',
    strokeWeight: 1,
    size: new google.maps.Size(60, 60),
    anchor: new google.maps.Point(38, 80)
  };

  for (var i = 0; i < mapPoints.length; i++) {
    var point = mapPoints[i];
    var marker = new google.maps.Marker({
      position: {lat: point[1], lng: point[2]},
      map: map,
      icon: icon,
      title: point[0],
      html: point[3],
    });
  }
}